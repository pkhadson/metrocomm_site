<?php
error_reporting(0);
include('admin/php_files/connect.php');
$metrocomm = array();
$metrocomm['title']="Metrocomm";
$metrocomm['url']="http://127.0.0.1/";

if(!$_GET['page']){
	$content_page= '__views/home.php';
}else{
	$content_page = '__views/'.$_GET['page'].'.php';
}
if(!file_exists($content_page)){
	$content_page= '__views/home.php';
}
?><!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8" />
	<title>Metrocomm</title>
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
	<style type="text/css">
		a[href='/<?=$_GET['page']; ?>'] li::before{
				width: 100% !important;
	opacity: 1 !important;
		}
	</style>
	<script src="//www.landmarkmlp.com/js-plugin/owl.carousel/assets/js/jquery-1.9.1.min.js"></script> 
</head>
<body>
<div class="bar-top">
	<div class="in">
		<ul class="redes">
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
			<a class="ic" href="##" target="_blank"><li></li></a>
		</ul>
		<div class="text-email"><a href="mainto:contato@metrocomm.com">contato@metrocomm.com</a></div>
	</div>
</div>
<div class="menu-top">
	<div class="in">
		<a href="#">
			<img src="images/logo.png" class="logo" alt="logo metrocomm" title="<?=$metrocomm['title'] ?>" />
			<h1 style="display:none"><?=$metrocomm['title']; ?></h1>
		</a>
		<ul class="menu">
			<a href="/"><li>FORMAS DE VENDER</li></a>
			<a href="/precos"><li>preços</li></a>
			<a href="##"><li>blog</li></a>
			<a href="/contato"><li>contato</li></a>
		</ul>
		<a href="##"><div class="button-login">login</div></a>
	</div>
</div>
<div class="content-page">
	<?php include($content_page); ?>
</div>
<div class="footer">
	<div class="in">
		<div class="collumn">
			<img class="logo-foot" src="images/logo.png" alt="logo metrocomm" title="<?=$metrocomm['title'] ?>" />
			<div class="col-content">Lorem ipsum dolor sit amet, consectetur 
				Adipiscing elit. fusce malesuada ante ac ante pharetra.
				Non condimentum dolor elementum. Praesent id nibh aliquam, commodo ipsum sit.
			</div>
		</div>
		<div class="collumn">
			<h3>ULTIMOS POST</h3>
			<ul class="col-list">
				<a href="##"><li>Lorem ipsum dolor sit amet, consectetur </li></a>
				<a href="##"><li>Lorem ipsum dolor sit amet, consectetur </li></a>
				<a href="##"><li>Lorem ipsum dolor sit amet, consectetur </li></a>
				<a href="##"><li>Lorem ipsum dolor sit amet, consectetur </li></a>
			</ul>
		</div>
		<div class="collumn">
			<h3>inscreva-se para newletter</h3>
			<div class="col-content form-newletter">
				<input type="text" placeholder="Seu email aqui" name="newletter.email" />
				<button class="button-submit-email">INSCREVA-SE AGORA</button>
			</div>
		</div>
	</div>
	<div class="foot">
		<div class="in">
			<div class="foot_text">Copyright © 2016 <a href="#" class="author">CriatiArt</a> |  <a href="##">Termos e suporte</a> |  <a href="##">Ajuda</a></div>
			<ul class="redes">
				<a href="##"><li></li></a>
				<a href="##"><li></li></a>
				<a href="##"><li></li></a>
			</ul>
		</div>
	</div>
</div>
</body>
</html>