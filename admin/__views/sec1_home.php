<?php
$rs = $con->query("SELECT * FROM sec1_home where id=1");
$row = (array)$rs->fetch();
$row['list']=json_decode($row['list']);
?><div class="card">
	<h2 class="title-card">Editar apresentação da pagina inicial</h2>
	<div class="card-content">
		<form name="form" method="POST" enctype="multipart/form-data">
			<div class="form-grup">
				<label>Titulo:</label>
				<input value="<?=$row['title']; ?>" placeholder="Digite aqui o titulo que aparecerá na pagina" required type="text" name="title"/>
			</div>
			<div class="form-grup">
				<label>Sub-titulo:</label>
				<input value="<?=$row['subtitle']; ?>" placeholder="Digite aqui o sub-titulo que aparecerá na pagina" required type="text" name="subtitle"/>
			</div>
			<div class="form-grup">
				<label>Lista:</label>
				<ul class="list"><?php foreach ($row['list'] as $line) {?>
					<input name=list[] type="text" value="<?=$line?>" style="margin-top:5px;" />
				<?php } ?>
				</ul>
				<span onclick="$('ul.list').append('<input name=list[] type=text style=margin-top:5px; />')">Adicionar mais uma frase</span>
			</div>
			<div class="form-grup">
				<label>Image:</label>
				<input type="file" name="image" accept="image/*" />
			</div>
			<div class="form-grup">
				<label>Fundo da section:</label>
				<input type="file" name="image2" accept="image/*" />
			</div>
			<div class="form-grup">
				<button class="submit">Salvar</button>
			</div>
		</form>
	</div>
</div>
<?php
if($_POST){
	$_POST['list']=json_encode($_POST['list']);
	$rs = $con->query("UPDATE `sec1_home` SET `title`='".$_POST['title']."',`subtitle`='".$_POST['subtitle']."',`list`='".$_POST['list']."' WHERE  `id`=1");
	//echo'<meta http-equiv="refresh" content="0"><script>alert("Editado com sucesso");</script>';
	$data = file_get_contents($_FILES['image']['tmp_name']);
	uploadFile($data, '../images/mac_site.png');
	$data = file_get_contents($_FILES['image2']['tmp_name']);
	uploadFile($data, '../images/top_bg.jpg');
}
?>