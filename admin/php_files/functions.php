<?php
function uploadFile($data, $path_file){
	if($data == ''){
		return 0;
	}
	$file = $path_file;
	$success = file_put_contents($file, $data);
	$data = base64_decode($data); 
	$source_img = imagecreatefromstring($data);
	$rotated_img = imagerotate($source_img, 90, 0); 
	$file = $path_file;
	if(strstr($path_file, '.jpg')){
		$imageSave = imagejpeg($rotated_img, $file, 10);
	}else{
		$imageSave = imagepng($rotated_img, $file, 10);
	}
	imagedestroy($source_img);
}