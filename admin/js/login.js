$(document).ready(function () {
    $('form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url : 'login_f.php',
            type: "POST",
            data: $(this).serialize(),
            success: function (data) {
                if(!data.login){alert('ID invalido!');return 0;}
                if(!data.pass){alert('Senha invalida!');return 0;}
                alert('Você será redirecionado!');
                window.location='index.php';
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert('ERRO: Entre em contato com um tecnico/desenvolvedor');
            }
        });
    });
});