<!DOCTYPE html>
<html ng-app="pz">
<head>
	<meta charset="utf-8" />
	<title>Painel Administrável</title>
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<script src="//www.landmarkmlp.com/js-plugin/owl.carousel/assets/js/jquery-1.9.1.min.js"></script> 
</head>
<body ng-controller="LoginCtrl">
<div class="box-login">
	<div class="header">Faça login</div>
	<form method="POST" class="form" ng-submit="funcLogin(login)">
		<div class="input-icon"></div><input required name="login" type="number" placeholder="Digite seu ID" />
		<div class="input-icon"></div><input required name="pass" type="password" placeholder="Digite sua senha" />
		<button>Logar</button>
	</form>
</div>
<div class="footer">
	©  <a href="//fb.com/pkhadson" target="_blank">Patrick Hadson</a>
</div>

<script type="text/javascript" src="js/login.js"></script>
</body>
</html>