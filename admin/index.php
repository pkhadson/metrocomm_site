<?php
include('php_files/connect.php');//INCLUI O CODIGO QUE CONECTA AO BANCO DE DADOS
session_start();//INCIIA A SESSAO
if(!$_SESSION['user']){
	header('Location: login.php');//REDIREIONA O USUARIO CASO NÃO ESTEJA LOGADO
}
$user=(array)json_decode($_SESSION['user']);
$page = str_replace(array("/admin/","admin/"),array('',''), $_SERVER['REQUEST_URI']);
$page_file = '__views/'.$page.'.php';
if(!file_exists($page_file)){
	$page_file='__views/home.php';
}
include('php_files/functions.php')
?><!DOCTYPE html>
<html ng-app="pz">
<head>
	<meta charset="utf-8" />
	<base href="/admin/">
	<title>Painel Administrável</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script src="//www.landmarkmlp.com/js-plugin/owl.carousel/assets/js/jquery-1.9.1.min.js"></script>

</head>
<body >
<div class="top">
	<div class="center">
		<span> Seja bem vindo, <b><?=$user['name']; ?> <?=$user['lastname']; ?></b> </span>
		<div class="photo" style="background: url('images/photos/<?=$user['id']; ?>.jpg')center;background-size: cover"></div>
	</div>
</div><div class="menu-left">
	<div class="menu-top">
		<div class="photo" style="background: url('images/photos/<?=$user['id']; ?>.jpg')center;background-size: cover"></div>
		<div class="name"><?=$user['name']; ?> <?=$user['lastname']; ?></div>
		<div class="rank">Administrador</div>
	</div>
	<ul>
		<a href="aa/.."><li><ic></ic><span>Pagina Inicial</span> </li></a>
		<a href="edit_profile"><li><ic></ic><span>Editar perfil</span> </li></a>
		<a href="sec1_home"><li><ic></ic><span>Apresentação Home</span> </li></a>
		
	</ul>
</div>

<div class="ng-view content"><?php include($page_file); ?></div>
<script type="text/javascript" src="js/panelPk.js"></script>
<div id="user" style="display:none"><?php echo$_SESSION['user']; ?></div>
</body>
</html>