<link rel="stylesheet" type="text/css" href="css/contato.css">
<script type="text/javascript" src="js/pages/contato.js"></script>
<section class="contato in" style="width: 1000px">
	<form autocomplete="off" method="POST" class="first">
		<div class="form_grup">
			<label>Nome<color1>*</color1></label>
			<input required type="text" name="contato.name">
		</div>
		<div class="form_grup">
			<label>Email<color1>*</color1></label>
			<input required type="email" name="contato.email">
		</div>
		<div class="form_grup">
			<label>Assunto<color1>*</color1></label>
			<input required type="text" name="contato.assunto">
		</div>
		<div class="form_grup">
			<label>Como posso ajudar?<color1>*</color1></label>
			<textarea required name="contato.message"></textarea>
		</div>
		<div class="form_grup">
			<button class="submit_contato">ENVIAR CONTATO</button>
		</div>
	</form>
	<div class="second">
		<h2>Bom ouvi de você</h2>
	</div>
</section>