<link rel="stylesheet" type="text/css" href="css/depoimentos.css" />
<script type="text/javascript" src="js/pages/depoimentos.js"></script>
<script src="//www.landmarkmlp.com/js-plugin/owl.carousel/owl-carousel/owl.carousel.js"></script>
<section class="depoimentos">
	<div class="in">
		<div id="owl-demo" class="owl-carousel owl-theme slide_depoimentos">
			<div class="item">
				<div class="image_photo">
					<img src="" alt="photo_depoimento" />
				</div>
				<p class="depoimento_text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</p>
				<div class="name">Lorem Ipsum</div>
				<div class="company">Empresa @@#$%$43</div>
			</div>
			<div class="item">
				<div class="image_photo">
					<img src="" alt="photo_depoimento" />
				</div>
				<p class="depoimento_text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</p>
				<div class="name">Lorem Ipsum</div>
				<div class="company">Empresa @@#$%$43</div>
			</div>
		</div>
	</div>
</section>


<link href="//www.landmarkmlp.com/js-plugin/owl.carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
