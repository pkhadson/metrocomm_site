<link rel="stylesheet" type="text/css" href="css/home.css" />
<script type="text/javascript" src="js/pages/home.js"></script>
<section class="home_1"><?php
$rs = $con->query("SELECT * FROM sec1_home where id=1");
$row = (array)$rs->fetch(); ?>
	<div class="in">
		<div class="first">
			<h1><?=$row['title']; ?></h1>
			<h2><?=$row['subtitle']; ?></h2>
			<ul class="list">
				<?php foreach (json_decode($row['list']) as $a) {
					echo"<li>".$a.'</li>';
				} ?>
			</ul>
			<div style="box-shadow:none;" class="form_start">
				<input style="display:none" type="text" name="name" />
				<button class="submit_start">COMEÇAR SEU TESTE GRÁTIS</button>
			</div>
		</div>
		<div class="image">
			<img src="images/mac_site.png" class="mac" alt="Mac <?=$metrocomm['title']; ?>"/>
		</div>
	</div>
</section>
<section class="collumns">
	<div class="col fist">
		<h3>ullamco laboris</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l abore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
	</div>
	<div class="col second">
		<h3>ullamco laboris</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l abore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
	</div>
	<div class="col third">
		<h3>ullamco laboris</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l abore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
	</div>
</section>
<section class="home_3">
	<div class="in">
		<div class="first">
			<img src="images/mac_site2.png" />
		</div>
		<div class="second">
			<h2>accusantium <c1>amet</c1></h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce malesuada ante ac ante pharetra, non condimentum dolor elementum. Praesent id nibh aliquam, commodo ipsum sit amet, feugiat velit. Praesent porttitor lectus sit amet nisl accumsan, in ullamcorper mauris gravida. </p>
			<ul class="list">
				<li>id nibh aliquam, commodo ipsum si</li>
				<li>id nibh aliquam, commodo ipsum si</li>
				<li>id nibh aliquam, commodo ipsum si</li>
				<li>id nibh aliquam, commodo ipsum si</li>
				<li>id nibh aliquam, commodo ipsum si</li>
			</ul>
		</div>
	</div>
</section>
<?php include('__views/depoimentos.php'); ?>